#!/bin/sh

cd /app
docker build -f Dockerfile -t geircode/learning_salt .
docker-compose -f docker-compose.yml up -d --build --remove-orphans
docker exec -it learning_salt-1 /bin/bash
