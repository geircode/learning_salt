import subprocess
from saltstack.DockerService import DockerService

class ScriptRunner:

    def runCommand(self, command, isCheck=True): 
        print("\nRunning: {}".format(command))
        completedProcess = subprocess.run(command, shell=True, check=isCheck)
        return completedProcess

    def docker_exec_it_bash(self):
        container = DockerService().getMinionContainers()[0]
        self.runCommand("docker exec -it {container_id} /bin/bash".format(container_id=container.id), isCheck=False)

if __name__ == '__main__':
    ScriptRunner().docker_exec_it_bash()