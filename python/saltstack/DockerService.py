import docker
import pydash

class DockerService:

    def __init__(self):
        self.client = docker.from_env()   

    def getContainers(self):
        containers = self.client.containers.list(all=True)
        return containers
        
    def getMinionContainers(self):
        containers = self.client.containers.list()
        name = "saltstack_minion_debian_saltstack_minion_debian"
        result = pydash.chain(containers)\
            .filter(lambda a: self.filterOnMinionName(container=a, name=name))\
            .sort(key=self.sortOnMinionName)\
            .value()
        return result

    def filterOnMinionName(self, container, name):
        return name in container.name

    def sortOnMinionName(self, container):
        return container.name
        
if __name__ == '__main__':
    obj = DockerService().getMinionContainers()
    for item in obj:
        print(item.name)
    pass