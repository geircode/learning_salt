import unittest
import datetime
from saltstack.DockerService import DockerService

class TestDockerService(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.docker_service = DockerService()
        
    def setUp(self):
        print(datetime.datetime.now())   

    def test_get_containers(self):
        container_list = self.docker_service.getContainers()
        print(container_list)

if __name__ == '__main__':
    unittest.main()
