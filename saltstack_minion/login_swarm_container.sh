#!/bin/bash

# Docker Swarm does not have a nice way to login into a arbituary container
# This function (script one-liner) gets the first container in the service and opens a terminal into it

if [ -z "$container_image_name" ]
then
      container_image_name=$1
fi

if [ -z "$container_image_name" ]
then
      container_image_name='saltstack_minion_debian_saltstack_minion_debian.1'
fi

container_image_name_id=$(docker service ps -f "name=$container_image_name" saltstack_minion_debian_saltstack_minion_debian -q --no-trunc | head -n1)

docker exec -it $container_image_name.$container_image_name_id /bin/bash