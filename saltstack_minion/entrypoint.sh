#!/bin/bash

# Configure the minion config file by replacing merging values from environment variables into the config
python /scripts/entrypoint.py

salt-minion --log-level=debug

tail -f /dev/null