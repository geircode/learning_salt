cd %~dp0

set DOCKER_IMAGE_BUILDER_NAME=docker_image_builder_minion-1

docker cp login_swarm_container.sh %DOCKER_IMAGE_BUILDER_NAME%:/scripts/login_swarm_container.sh
docker exec -it %DOCKER_IMAGE_BUILDER_NAME% dos2unix /scripts/login_swarm_container.sh
docker exec -it %DOCKER_IMAGE_BUILDER_NAME% /scripts/login_swarm_container.sh saltstack_minion_debian_saltstack_minion_debian.1

pause