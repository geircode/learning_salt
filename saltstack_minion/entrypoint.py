import time
import os
import socket    

class FileService:
    def store_text_to_file(self, filepath, filecontent):
        with open(filepath, 'w+') as the_file:
            the_file.write(filecontent)

    def get_file_content(self, filepath):
        filecontent = None
        with open(filepath, 'r') as the_file:
            filecontent = the_file.read()
        return filecontent

class Entrypoint:

    def __init__(self):
        self.fileService = FileService()

    def replace_env_variables_in_minion_config_file(self, minion_filepath):
        content = self.fileService.get_file_content(minion_filepath)
        env_location_salt_master_server = os.getenv("LOCATION_SALT_MASTER_SERVER")
        location_salt_master_server = "localhost"

        if(env_location_salt_master_server != None):            
            location_salt_master_server = socket.gethostbyname(env_location_salt_master_server)
            print("#### : " + location_salt_master_server)

        content = content.replace("LOCATION_SALT_MASTER_SERVER", location_salt_master_server)
        
        self.fileService.store_text_to_file(minion_filepath, content)

if __name__ == '__main__':  
    start_time = time.time()
    minion_filepath = os.getenv("MINION_FILEPATH", "saltstack_minion/etc/salt/minion")
    Entrypoint().replace_env_variables_in_minion_config_file(minion_filepath)

    print("--- {0} seconds ---".format(time.time() - start_time))