#!/bin/bash

# Docker Swarm does not have a nice way to login into a arbituary container
# This function (script one-liner) gets the first container in the service and opens a terminal into it
docker exec -it saltstack_minion_debian_saltstack_minion_debian.1.$(docker service ps -f 'name=saltstack_minion_debian_saltstack_minion_debian.1' saltstack_minion_debian_saltstack_minion_debian -q --no-trunc | head -n1) /bin/bash