cd %~dp0

set DOCKER_IMAGE_BUILDER_NAME=docker_image_builder_minion-1

REM Build within a Linux Container to get the CR/LF line endings to be correct when building the Linux Container Image
docker rm -f %DOCKER_IMAGE_BUILDER_NAME%
docker-compose -f docker-compose.linux.build.yml down
docker-compose -f docker-compose.linux.build.yml up -d 

docker exec -it %DOCKER_IMAGE_BUILDER_NAME% /scripts/copy_and_convert_app_folder.sh
docker exec -it %DOCKER_IMAGE_BUILDER_NAME% docker-compose -f /app-copy/docker-compose.yml build

REM Start the container image

REM Configure Docker to become a single node Swarm
docker swarm init
docker stack rm saltstack_minion_debian
docker-compose -f docker-compose.yml build
docker stack deploy -c docker-compose.yml saltstack_minion_debian
pause
call docker-compose.bash.swarm.bat