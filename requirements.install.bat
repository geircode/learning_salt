cd %~dp0
virtualenv .venv
call .venv\Scripts\activate
pip install -U -r "%~dp0requirements.txt"
pause
