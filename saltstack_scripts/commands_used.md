

# Tester Grains

## minion
Setter grain på minion manuelt/direkte:
```
salt-call grains.set roles geircode-web
cat /etc/salt/grains

```

## master

List ut alle minions som har denne "grain.item" og hvilken verdi den har:
```
salt '*' grains.item roles
```

Oppdater alle minions med denne "grain.item"
```
salt '*' grains.set roles geircode-web
```



