# Learning Saltstack

## Install Saltstack on Debian (Buster)
https://docs.saltstack.com/en/latest/topics/installation/debian.html

Install instructions

- Go to https://repo.saltstack.com/#debian:

![salt-install-buster](wiki\files\salt-install-buster.png)

I used this documentation to create the [Saltstack Dockerfile](saltstack\Dockerfile). Fun stuff though, since the wget link above did not work when I tried it. Anyways, I managed to get it to work.

## Automation-First
The Dockerfile contains everything to install Saltstack during Container Image build.

```dockerfile
RUN apt-get update
RUN apt-get install -y salt-minion salt-api salt-cloud salt-master salt-minion salt-ssh salt-syndic
```

The Dockerfile for creating the Saltstack Container Image is in the folder [/saltstack/Dockerfile](saltstack\Dockerfile)

## Learning issues 

### Problems with the current running user on master:

![dmidecode_found_cannot_execute](wiki\files\dmidecode_found_cannot_execute.png)

The solution was to add this to the "**/etc/salt/master**" config:

In the "master" file, the following has been changed to:

- user : root

```
# The user under which the salt master will run. Salt will update all
# permissions to allow the specified user to run the master. The exception is
# the job cache, which must be deleted if this user is changed. If the
# modified files cause conflicts, set verify_env to False.
user: root
```

Now, the running user is able to use [dmidecode](https://linux.die.net/man/8/dmidecode)

#### Minion unable to connect to the master
Because it tries to connect to the default "salt" instead of "localhost" in our case, and had to change the config of the [minion](saltstack\etc\salt\minion) file:
- master: localhost

```
# Set the location of the salt master server. If the master server cannot be
# resolved, then the minion will fail to start.
master: localhost
```

#### Auto accept minions to the master

Next step was to manually accept each minions as they started up:

<img style="" src="wiki\files\1570800266795.png">



And this is not very Automation-First friendly, so how can I automate this?

https://docs.saltstack.com/en/latest/topics/tutorials/autoaccept_grains.html

```
autosign_grains_dir: /etc/salt/autosign_grains
```





<img src="wiki\files\Ranting-Homer.jpg"
     style="float: left; height: 60px;" />

**Rant**: Of course, the documentation does not actually say explicit how to do configure this. Why can't the docs be explicit with lots of examples such as how Azure, AWS or Google document their SDK? Why do I need spend time guessing how to do this? It is more secure to explicit how to do this correctly.

I actually did not find any working examples on Google. I found some 'hacks' that consisted of a script that ran each second and accepts all the pending keys, but that's not a good solution. This needs to be declarative and automatic. The I managed to find a book => https://www.amazon.com/Salt-Cookbook-Anirban-Saha/dp/1784399744 that helped me some more. BUT why do I need to buy a book in order to figure out how to manage Salt? Why is this information not in the Salt documentation!? *frustrated*

**Yay**! I found the setting in the master file by accident:

```
# Enable auto_accept, this setting will automatically accept all incoming
# public keys from the minions. Note that this is insecure.
auto_accept: True
```

This setting was not mentioned in the book above and not in any of the hits I found on Google. It's even hard to find this setting on Salt's own websites => https://cse.google.com/cse?cx=011515552685726825874:ht0p8miksrm&q=auto_accept

### Success!

Salt cheat sheet:

https://dev-eole.ac-dijon.fr/doc/cheatsheets/saltstack.html

```bash
root@d907cc48b592:/scripts# salt-key --list all
Accepted Keys:
d907cc48b592
Denied Keys:
Unaccepted Keys:
Rejected Keys:

root@d907cc48b592:/scripts# salt '*' test.ping
d907cc48b592:
    True
root@d907cc48b592:/scripts#
```

This is showing that the one Minion is connected on the Master.

## What Next?

### Execution Modules
https://docs.saltstack.com/en/latest/ref/modules/all/index.html

Examples:

https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.cmdmod.html
```
salt '*' cmd.run 'ls -al'
```
https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.test.html
```
salt '*' test.ping
```

### Installing nodejs-package into a minion
First get a minionId from running 
```
salt-key
```

![1571388600869](wiki\files\1571388600869.png)

I created the file at /srv/salt/top.sls
```
base:
  '4c5ab02ee690':
    - nodejs-package
```

And /srv/salt/nodejs-package.sls
```
nodejs:
  pkg:
    - installed
```

and run this command on the salt master:
```bash
salt '4c5ab02ee690' state.apply
```

Since we are running minions in Containers, we will get a new hostname each time unless we set it in the docker-compose file. This however is not possible since we are running a Docker Swarm based on the same configuration. So, we need another way to target specific type of containers based on environment variable or something similar.

#### Interesting bug with Docker Desktop and date/time drifting

I tried to apply state, but got this error:
```
root@e015ccacadcf:/# salt '4c5ab02ee690' state.apply
4c5ab02ee690:
----------
          ID: nodejs
    Function: pkg.installed
      Result: False
     Comment: An error was encountered while installing package(s): E: Release file for http://security.debian.org/debian-security/dists/buster/updates/InRelease is not valid yet (invalid for another 1h 48min 49s). Updates for this repository will not be applied.
     Started: 05:32:51.074185
    Duration: 2322.1 ms
     Changes:

Summary for 4c5ab02ee690
------------
Succeeded: 0
Failed:    1
------------
Total states run:     1
Total run time:   2.322 s
ERROR: Minions returned with non-zero exit code
```

And since I am doing stuff through Salt, I can also check the date on all running containers using the command
```bash
root@e015ccacadcf:/# salt '*' cmd.run 'date'
aec068c7f1f2:
    Fri Oct 18 05:34:57 UTC 2019
4c5ab02ee690:
    Fri Oct 18 05:34:57 UTC 2019
df0998d44bc8:
    Fri Oct 18 05:34:57 UTC 2019
f45e04759baf:
    Fri Oct 18 05:34:57 UTC 2019
3e1d1bb0c88c:
    Fri Oct 18 05:34:57 UTC 2019
```

This was wrong according to the date on the computer that was about Fri Oct 18 07:**55**:14 UTC 2019

Turns out that I had to reset the Hyper-V time sync:

```
Disabling and then enabling again the MobyLinuxVM > Settings > Integration Services > Time synchronization service from Hyper-V Manager resyncs the clock of containers immediately.
```

**Moving on ...**

Trying again got me this:

![1571390256312](wiki\files\1571390256312.png)

Yay. Something happened.

Checking that something actually got installed into the container:

```bash
docker exec -it 4c5ab02ee690 /bin/bash
root@4c5ab02ee690:/# nodejs --version
v10.15.2
```
Nodejs has been installed!

Of course, in a Container world the installation of any software would have been done when building the Container Image and not by running any salt commands. Also, the configuration of the Container Image would be stored and maintained in the Container Plattform Orchestrator, and not by SaltStack.

So this how-to/guide is about learning how to use SaltStack, and not how to use SaltStack on the Container technology.

Perhaps SaltStack can be used as a Container Orchestrator when using normal standalone Linux Servers together with Docker Containers?
Or use SaltStack to create Container Images instead of maintaining Linux Servers VM's? One step in the rigth direction for someone that is now only using Linux VM's?

Imagine replacing existing Linux VM's with Linux Containers running on new Linux VM's or in a Kubernetes cluster.

**Ok, moving on again...**

To see more of the stuff going on beneath the carpet, you can tell the command to be very verbose:

```
salt '4c5ab02ee690' state.apply --verbose -l all
```

### Create new user

New files:

/srv/salt/geircode-site.sls
```yaml
geircode-user:
  user.present:
    - name: geircode
    - home: /home/geircode
```

Updated: /srv/salt/top.sls
```yaml
base:
  '4c5ab02ee690':
    - nodejs-package
    - geircode-site
```

Run this command to apply the changes:
```
salt '4c5ab02ee690' state.apply
```

**Discovery**
Change the name of the user to something else i.e => "geircode123" and apply the changes again and check the /etc/password file. This will not in fact change the current user, but create another user. Nor will it delete the previous user. SaltStack is not keeping 'state', but only try to create new states disregarding previous states. That's ok in it self, but this reduces StackStack to 'just' being a very fancy script-runner against a horde of Virtual Machines.

But it's possible to change the "home" variable in order to update the home directory of the "geircode" user.

```
root@4c5ab02ee690:/usr# cat /etc/passwd
...
geircode:x:1000:1000::/home/geircode:/bin/sh
geircode123:x:1001:1001::/home/geircode321:/bin/sh
root@4c5ab02ee690:/usr#  
```


