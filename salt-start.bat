cd %~dp0

docker rm -f learning_salt-1
docker-compose -f docker-compose.yml up -d

start cmd /C docker exec -it learning_salt-1 salt-master --log-level=debug 
start cmd /C docker exec -it learning_salt-1 salt-minion --log-level=debug
REM pause