cd %~dp0

docker rm -f docker_image_builder-1
docker rm -f saltstack_master_debian-1
docker rm -f saltstack_minion_saltstack_minion_debian_1

docker network create -d overlay --attachable saltstack_master_debian_network

start cmd /C saltstack_master\docker-compose.linux.build.up.bat
start cmd /C saltstack_minion\docker-compose.linux.build.up.bat

pause
start cmd /C docker exec -it saltstack_master_debian-1 salt-master --log-level=debug 
start cmd /C docker exec -it saltstack_minion_saltstack_minion_debian_1 salt-minion --log-level=debug

pause
start "saltstack_master" cmd saltstack_master_debian /C docker exec -it saltstack_master_debian-1 /bin/bash
start "saltstack_minion" cmd /C docker exec -it saltstack_minion_saltstack_minion_debian_1 /bin/bash
