# Configuration

In the "master" file, the following has been changed to:

- user : root

```
# The user under which the salt master will run. Salt will update all
# permissions to allow the specified user to run the master. The exception is
# the job cache, which must be deleted if this user is changed. If the
# modified files cause conflicts, set verify_env to False.
user: root
```

Now, correct user is running salt and is able to use what-serv.
