nodejs-repo:
  pkgrepo.managed:
    - humanname: NodeJS nodesource repo
    - name: deb https://deb.nodesource.com/node_13.x buster main
    - key_url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key

nodejs:
  pkg:
    - installed
    - downloaded:
      - version: v13.0.1
    - require:
      - pkgrepo: nodejs-repo