include:
  - geircode-site

{# supervisor_package:
  pkg.installed:
    - name: supervisor  
    - require:
      - sls: geircode-site

supervisor:
  service.running:
    - watch:
      - file: /etc/supervisor/conf.d/geircode-site.conf
    - require:
      - pkg: supervisor_package


geircode-site.conf:
  file.managed:
    - name: /etc/supervisor/conf.d/geircode-site.conf
    - source: salt://geircode-service/supervisor.conf
    - require:
      - pkg: supervisor_package #}

run_node:
  cmd.run:
    - name: nohup node /app/. & 
    - runas: geircode

{# run_node:
  cmd.run:
    - name: node /app/.
    - runas: geircode #}

{# supervisor:
  pkg.installed:
    - require:
      - sls: hwaas-site
  service.running:
    - watch:
      - file: /etc/supervisor/conf.d/hwaas-site.conf

/etc/supervisor/conf.d/hwaas-site.conf:
  file.managed:
    - source: salt://hwaas-service/supervisor.conf
    - require:
      - pkg: supervisor #}
