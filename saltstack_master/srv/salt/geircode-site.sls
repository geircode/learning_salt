include:
  - nodejs-package

geircode_user:
  user.present:
    - name: geircode
    - home: /home/geircode

git_client_package:
  pkg.installed:
    - name: git

geircode_source:
  git.latest:
    {# - name: https://bitbucket.org/geircode/portainer/src/master/ #}
    - name: https://github.com/floyd-may/hwaas.git
    - rev: master
    - target: /app/
    - require:
      - user: geircode_user
      - pkg: git_client_package
      - sls: nodejs-package

geircode_npm_install:
  cmd.wait:
    - name: npm install
    - cwd: /app
    - watch:
      - git: geircode_source

geircode_build_script:
  cmd.wait:
    - name: npm run-script build
    - cwd: /app
    - watch:
      - git: geircode_source      