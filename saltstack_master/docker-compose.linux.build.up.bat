cd %~dp0

docker network create -d overlay --attachable saltstack_master_debian_network

set DOCKER_IMAGE_BUILDER_NAME=docker_image_builder_master-1

REM Build within a Linux Container to get the CR/LF line endings to be correct when building the Linux Container Image
docker rm -f %DOCKER_IMAGE_BUILDER_NAME%
docker-compose -f docker-compose.linux.build.yml down
docker-compose -f docker-compose.linux.build.yml up -d 

docker exec -it %DOCKER_IMAGE_BUILDER_NAME% /scripts/copy_and_convert_app_folder.sh
docker exec -it %DOCKER_IMAGE_BUILDER_NAME% docker-compose -f /app-copy/docker-compose.yml build

REM Start the container image
REM Configure Docker to become a single node Swarm
docker swarm init
docker-compose -f docker-compose.yml up
pause