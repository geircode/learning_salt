import time
import os
import socket    

class FileService:
    def store_text_to_file(self, filepath, filecontent):
        with open(filepath, 'w+') as the_file:
            the_file.write(filecontent)

    def get_file_content(self, filepath):
        filecontent = None
        with open(filepath, 'r') as the_file:
            filecontent = the_file.read()
        return filecontent

class Entrypoint:

    def __init__(self):
        self.fileService = FileService()

    def replace_env_variables_in_the_config_file(self, config_filepath):
        content = self.fileService.get_file_content(config_filepath)
        content = content.replace("FILE_ROOTS_BASE", os.getenv("FILE_ROOTS_BASE"))
        self.fileService.store_text_to_file(config_filepath, content)

if __name__ == '__main__':  
    start_time = time.time()
    config_filepath = os.getenv("MASTER_FILEPATH", "saltstack_master/etc/salt/master")
    Entrypoint().replace_env_variables_in_the_config_file(config_filepath)

    print("--- {0} seconds ---".format(time.time() - start_time))