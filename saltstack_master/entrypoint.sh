#!/bin/bash

# Configure the minion config file by replacing merging values from environment variables into the config
python /scripts/entrypoint.py

salt-master --log-level=debug

tail -f /dev/null