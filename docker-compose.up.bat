cd %~dp0
docker rm -f learning_salt-1
docker-compose -f docker-compose.yml down --remove-orphans
REM docker-compose -f docker-compose.yml pull 
docker-compose -f docker-compose.yml up -d  --build --remove-orphans
REM wait for 1-2 seconds for the container to start
pause
docker exec -it learning_salt-1 /bin/bash