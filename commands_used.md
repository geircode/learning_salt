# Commands used in this setup

## To see which Distro this container is running:
https://linuxconfig.org/check-what-debian-version-you-are-running-on-your-linux-system

cat /etc/os-release
apt-get update
apt-get install lsb-release

salt-minion --versions-report

service salt-master start
service salt-minion start

service salt-master stop
service salt-minion stop --log-level=debug

## Start directly

salt-master --log-level=debug
salt-minion --log-level=debug

salt-key -a 094b9656f128

## Salt CLI commands

salt '*' test.ping
salt '*' test.version

## Targeting

https://docs.saltstack.com/en/latest/topics/targeting/index.html#targeting

To discover what grains are available and what the values are, execute the grains.item salt function:
```
salt '*' grains.items
salt -G 'os:Debian' test.version
```

## More commands

Master
```
salt-key
salt-key -f efdca26c75d0
salt '*' test.ping
salt '*' state.apply
```

Minion
```
salt-call --local key.finger
```

## git.latest
https://docs.saltstack.com/en/latest/ref/states/all/salt.states.git.html
https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.git.html

## Supervisor - Python
http://supervisord.org/
```
Supervisor is a client/server system that allows its users to monitor and control a number of processes on UNIX-like operating systems. 
```
But we will get this error:
```
    ID: supervisor
    Function: service.running
      Result: False
     Comment: Unable to run command '['runlevel']' with the context '{'cwd': '/root', 'shell': False, 'env': {'HOSTNAME': '22936adb0968', 'PYTHON_VERSION': '3.6.9', 'PWD': '/', 'HOME': '/root', 'LANG': 'C.UTF-8', 'GPG_KEY': '0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D', 'MINION_FILEPATH': '/etc/salt/minion', 'SHLVL': '1', 'PYTHON_PIP_VERSION': '19.3.1', 'PYTHON_GET_PIP_SHA256': 'b86f36cc4345ae87bfd4f10ef6b2dbfa7a872fbff70608a1e43944d283fd0eee', 'PYTHON_GET_PIP_URL': 'https://github.com/pypa/get-pip/raw/ffe826207a010164265d9cc807978e3604d18ca0/get-pip.py', 'LOCATION_SALT_MASTER_SERVER': 'saltstack_master_debian', 'PATH': '/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin', '_': '/usr/bin/salt-minion', 'APT_LISTBUGS_FRONTEND': 'none', 'APT_LISTCHANGES_FRONTEND': 'none', 'DEBIAN_FRONTEND': 'noninteractive', 'UCF_FORCE_CONFFOLD': '1', 'LC_CTYPE': 'C', 'LC_NUMERIC': 'C', 'LC_TIME': 'C', 'LC_COLLATE': 'C', 'LC_MONETARY': 'C', 'LC_MESSAGES': 'C', 'LC_PAPER': 'C', 'LC_NAME': 'C', 'LC_ADDRESS': 'C', 'LC_TELEPHONE': 'C', 'LC_MEASUREMENT': 'C', 'LC_IDENTIFICATION': 'C', 'LANGUAGE': 'C'}, 'stdin': None, 'stdout': -1, 'stderr': -2, 'with_communicate': True, 'timeout': None, 'bg': False, 'close_fds': True}', reason: [Errno 2] No such file or directory: 'runlevel': 'runlevel': runlevel 
```

... because Docker Containers are not meant to run 'services' or change runlevels. Normally, in a Container we start the one main process at the ENTRYPOINT. A Container will always just do ONE thing, and is not supposed to run alot of services.

asd