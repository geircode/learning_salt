cd %~dp0
cd ..

call filecontainer\docker-compose.build.bat

cd ../%~dp0
call docker_image_builder\docker-compose.build.bat

cd %~dp0
docker rm -f docker_image_builder-1
docker rm -f saltstack_master_debian-1
docker rm -f saltstack_minion_saltstack_minion_debian_1

start cmd /C saltstack_master\docker-compose.linux.build.up.bat
start cmd /C saltstack_minion\docker-compose.linux.build.up.swarm.bat
