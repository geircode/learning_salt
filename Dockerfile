FROM geircode/learning_salt-filecontainer:latest as filecontainer

FROM geircode/saltstack_debian:latest

WORKDIR /app
COPY . /app

# RUN pip install -r requirements.txt 

# RUN apt-get install -y dos2unix virt-what

# RUN curl -fsSLO https://get.docker.com/builds/Linux/x86_64/docker-17.03.1-ce.tgz && tar --strip-components=1 -xvzf docker-17.03.1-ce.tgz -C /usr/local/bin

# WORKDIR /files
# COPY --from=filecontainer /files .
# RUN tar --strip-components=1 -xvzf docker-18.06.1-ce.tgz -C /usr/local/bin
# RUN rm *

# WORKDIR /saltstack
# RUN wget -O install-salt.sh  https://bootstrap.saltstack.com 

RUN mkdir /scripts
COPY entrypoint.sh /scripts
RUN chmod +x /scripts/entrypoint.sh
# RUN chmod 777 /scripts/entrypoint.sh

WORKDIR /scripts
RUN find . -type f -print0 | xargs -0 dos2unix

# COPY saltstack/etc/salt/master /etc/salt/master
# COPY saltstack/etc/salt/minion /etc/salt/minion
# COPY saltstack/etc/salt/autosign_grains/uuid etc/salt/autosign_grains/uuid

# RUN find /etc/salt/ -type f -print0 | xargs -0 dos2unix


# ENTRYPOINT tail -f /dev/null
ENTRYPOINT ["/scripts/entrypoint.sh"]
